from rest_framework.test import APITestCase, APIClient
from django_test import settings


class ApiResponseTestCase(APITestCase):

    def test_response_file_post(self):
        client = APIClient()
        data = {
            'level': '1',
            'file': open(settings.BASE_DIR + '/ecommerce/tests/api/data.json',
                         'r')
        }

        res = client.post('/api/level/', data, format='multipart')
        self.assertEqual(
            res.data, {
                'carts': [{
                    'id': 1,
                    'total': 2000
                }, {
                    'id': 2,
                    'total': 1400
                }, {
                    'id': 3,
                    'total': 0
                }]
            })
