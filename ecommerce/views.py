import json
from rest_framework.response import Response
from rest_framework.views import APIView

from ecommerce.levels.level1 import LevelOne
from ecommerce.levels.level2 import LevelTwo
from ecommerce.levels.level3 import LevelThree


class Level(APIView):
    def post(self, request, format=None):
        data = request.data
        result = ''
        file = json.loads(request.FILES['file'].read().decode('utf-8'))
        if data['level'] == '1':
            result = LevelOne(file)
        elif data['level'] == '2':
            result = LevelTwo(file)
        elif data['level'] == '3':
            result = LevelThree(file)
        return Response(result.process_order())
