## Running the post with the data via curl:

### Level1
```
curl -X POST  http://127.0.0.1:8000/api/level/  -H 'Cache-Control: no-cache'  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' -F level=1 -F 'file=@data.json';
```

### Level2
```
curl -X POST  http://127.0.0.1:8000/api/level/  -H 'Cache-Control: no-cache'  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' -F level=2 -F 'file=@data.json';
```

### Level2
```
curl -X POST  http://127.0.0.1:8000/api/level/  -H 'Cache-Control: no-cache'  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' -F level=3 -F 'file=@data.json';
```

## Running tests:
```
python manage.py test
```
